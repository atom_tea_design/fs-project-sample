import React from "react";
import { Route } from "react-router-dom";

import Navbar from "./Navbar";
import Callback from "./Callback";
import Users from "./Users";
import SomeForm from "./SomeForm";
import Footer from "./Footer";

import "./css/App.css";

function App() {
  return (
    <div className="App">
      <Navbar />
      <main>
        <Route exact path="/callback" component={Callback} />
        <Route exact path="/users" component={Users} />
        <Route path="/some-form" component={SomeForm} />
      </main>
      <Footer />
    </div>
  );
}

export default App;
