import React, { createRef } from "react";

class SomeForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      myInput: ""
    };

    this.myOtherInput = createRef();
  }

  formSubmit = event => {
    event.preventDefault();

    const myInput = this.state.myInput;
    const myOtherInput = this.myOtherInput.current.value;

    console.log({ myInput, myOtherInput });
  };

  render() {
    return (
      <form onSubmit={this.formSubmit}>
        <div>
          <input
            type="text"
            value={this.state.myInput}
            onChange={event => this.setState({ myInput: event.target.value })}
          />
        </div>
        <div>
          <input
            type="text"
            value={this.state.myInput}
            onChange={event => this.setState({ myInput: event.target.value })}
          />
        </div>
        <div>
          <input type="text" ref={this.myOtherInput} />
        </div>
        <button type="submit">SUBMIT FORM</button>
      </form>
    );
  }
}

export default SomeForm;
