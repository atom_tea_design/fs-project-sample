import React, { useState, useEffect } from "react";

import NewUserForm from "./NewUserForm";
import UsersList from "./UsersList";

import css from "./css/Users.css";
import auth0Client from "./Auth";

const Users = () => {
  const [data, setData] = useState({ users: [] });
  const [selectedUser, setSelectedUser] = useState(null);

  const fetchData = async () => {
    const response = await fetch("http://localhost:8280/users");
    const users = await response.json();
    setData({ users });
    setSelectedUser(null);
  };

  const onUserSelected = user => {
    setSelectedUser(user);
  };

  const onUserSaved = () => {
    fetchData();
  };

  const deleteUser = async userId => {
    const authToken = auth0Client.getAccessToken();
    console.log("access token:", authToken);
    const response = await fetch(`http://localhost:8280/users/${userId}`, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${authToken}`
      }
    });

    const status = response.status;

    if (status !== 200) {
      console.error("Delete user failed");
      return;
    }
    fetchData();
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="user-component">
      <NewUserForm onUserSaved={onUserSaved} user={selectedUser} />
      <UsersList
        users={data.users}
        onDeleteUser={deleteUser}
        onUserSelected={onUserSelected}
      />
    </div>
  );
};

export default Users;
