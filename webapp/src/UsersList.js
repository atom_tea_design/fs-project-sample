import React from "react";
import authClient from "./Auth.js";

import css from "./css/UsersList.css";

const User = ({ user, onDeleteClick, onRowClicked }) => {
  return (
    <tr onClick={onRowClicked}>
      <td>{user.email}</td>
      <td>{user.handle}</td>
      <td>{new Date(user.created_date).toLocaleDateString()}</td>
      <td>
        {authClient.isAuthenticated() && (
          <button
            type="button"
            className="btn btn-lin"
            onClick={event => {
              event.stopPropagation();
              onDeleteClick();
            }}
          >
            DELETE
          </button>
        )}
      </td>
    </tr>
  );
};

const UsersList = ({ users, onDeleteUser, onUserSelected }) => {
  return (
    <div className="users">
      <table className="user-table">
        <thead>
          <tr>
            <th>Email</th>
            <th>Handle</th>
            <th>Created</th>
            <th />
          </tr>
        </thead>
        <tbody>
          {users.map(u => (
            <User
              key={u.id}
              user={u}
              onDeleteClick={e => onDeleteUser(u.id)}
              onRowClicked={() => onUserSelected(u)}
            />
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default UsersList;
