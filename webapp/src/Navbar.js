import React from "react";
import { Link, withRouter } from "react-router-dom";

import auth0Client from "./Auth";

function Navbar(props) {
  const signOut = () => {
    auth0Client.signOut();
    props.history.replace("/");
  };

  return (
    <nav>
      <Link to="/users">USERS</Link>
      {!auth0Client.isAuthenticated() && (
        <button
          onClick={() => {
            auth0Client.signIn();
          }}
        >
          SIGN IN
        </button>
      )}
      {auth0Client.isAuthenticated() && (
        <div>
          <label>{auth0Client.getProfile.name}</label>
          <button
            onClick={() => {
              signOut();
            }}
          >
            SIGN OUT
          </button>
        </div>
      )}
    </nav>
  );
}

export default withRouter(Navbar);
