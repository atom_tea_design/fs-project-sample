import auth0 from "auth0-js";

class Auth {
  constructor() {
    this.auth0 = new auth0.WebAuth({
      clientID: "k3dNBV65Y065CCJKD7UyVBwK1lkLjPOQ",
      domain: "dev-vjv9tf4g.auth0.com",
      responseType: "token id_token",
      audience: "https://fs-project-sample/",
      redirectUri: "http://localhost:3333/callback",
      scope: "openid profile email"
    });
  }

  getProfile = () => {
    return this.profile;
  };

  getIdToken = () => {
    return this.idToken;
  };

  getAccessToken = () => {
    return this.accessToken;
  };

  isAuthenticated = () => {
    return new Date().getTime() < this.expiresAt;
  };

  signIn = () => {
    this.auth0.authorize();
  };

  handleAuthentication = () => {
    return new Promise((resolve, reject) => {
      this.auth0.parseHash((err, authResult) => {
        if (err) return reject(err);
        if (!authResult || !authResult.idToken) {
          return reject(err);
        }
        this.idToken = authResult.idToken;
        this.profile = authResult.idTokenPayload;
        this.accessToken = authResult.accessToken;
        // set the time at which the id token will expire
        this.expiresAt = authResult.idTokenPayload.exp * 1000;
        resolve();
      });
    });
  };

  signOut = () => {
    this.idToken = null;
    this.profile = null;
    this.expiresAt = null;
  };
}

const auth0Client = new Auth();

export default auth0Client;
