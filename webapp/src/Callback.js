import React, { useEffect } from "react";
import { withRouter } from "react-router-dom";
import auth0Client from "./Auth";

const Callback = ({ history }) => {
  useEffect(() => {
    function handleAuth() {
      auth0Client.handleAuthentication().then(() => {
        history.replace("/");
      });
    }

    handleAuth();
  }, [history]);

  return <p>LOADING PROFILE...</p>;
};

export default withRouter(Callback);
