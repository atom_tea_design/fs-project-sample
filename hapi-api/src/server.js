import Hapi from '@hapi/hapi'
import Relish from 'relish'
import jwksRsa, { JwksRateLimitError } from 'jwks-rsa'

import routes from './routes'
import configPlugins from './config/plugins'
import { bootstrap } from './db'
import dbClient from './db/knex'

const port = Number(process.argv[2]) || process.env.PORT || 8280
process.env.NODE_ENV = process.env.NODE_ENV || 'development'

const relish = Relish()

const testDbConnection = async () => {
  const results = await dbClient.raw('select 1')
  console.log('testDbConnection results: ', results)
}

// const validateUser = (decoded, request, callback) => {
//   if (decoded) {
//     if (decoded.scope && decode.sub) {
//       return callback(null, true, {
//         scope: decoded.scope.split(' '),
//       })
//     }
//     return callback(null, true)
//   }
//   return callback(null, false)
// }

const validateUser = (decoded, request) => {
  if (decoded && decoded.sub) {
    if (decoded.scope) {
      return {
        isValid: true,
        credentials: {
          scope: decoded.scope.split(' '),
        },
      }
    }
    return { isValid: true }
  }
  return { isValid: false }
}

export default async () => {
  const server = Hapi.Server({
    port: port,
    router: {
      isCaseSensitive: false,
      stripTrailingSlash: true,
    },
    routes: {
      cors: true,
      validate: {
        failAction: relish.failAction,
      },
    },
  })

  await server.register(require('hapi-auth-jwt2'))

  server.auth.strategy('jwt', 'jwt', {
    cvomplete: true,
    key: jwksRsa.hapiJwt2KeyAsync({
      cache: true,
      rateLimit: true,
      jwksRequestsPerMinute: 5,
      jwksUri: `https://${process.env.AUTH_DOMAIN}/.well-known/jwks.json`,
    }),
    verifyOptions: {
      audience: 'https://fs-project-sample-api',
      issuer: `https://${process.env.AUTH_DOMAIN}/`,
      algorithms: ['RS256'],
    },
    validate: validateUser,
  })

  await bootstrap.run()

  await testDbConnection()

  await configPlugins(server)

  server.route(routes)

  await server.initialize()

  return server
}
