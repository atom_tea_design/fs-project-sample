export const up = (knex) => {
  console.log(`running up for 00000_initial_setup.js`)
  return Promise.all(
    [
      knex.schema.hasTable('user').then(exists => {
        if(!exists) {
          return knex.schema.createTable('user', table => {
            table.uuid('id').primary()
            table.string('email', 256)
            table.string('handle', 100)
            table.specificType('created_date', 'timestamptz')
          })
        }
      }),
    ]
  )
}

export const down = (knex) => {
  return Promise.all([
    knex.schema.dropTable('user')
  ])
}
